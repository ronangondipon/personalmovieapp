const panels=document.querySelectorAll('.panel')
panels.forEach(panel =>{
    panel.addEventListener('click',()=>{
        removeActiveClasses()
        panel.classList.add('active')
    })
})

function removeActiveClasses(){
    panels.forEach(panel=>{
        panel.classList.remove('active')
    })
}

function goToNewPage(){
    var url=document.getElementById('list').ariaValueMax;
    if(url!='none'){
        window.location=url;
    }
}